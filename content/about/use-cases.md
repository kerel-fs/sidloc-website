---
title: "Use Cases"
date: 2021-06-30T13:48:14+03:00
---

The users and use cases of SIDLOC fall into two main high-level categories:

## Spacecraft Operators ##

For the spacecraft operator, the main use cases for SIDLOC are spacecraft identification during Launch and Early Orbit Phase, and monitoring of the evolution of the spacecraft's orbit during the whole duration of the mission.
Although radar-based localisation is usually sufficient to determine the spacecraft’s orbit within a couple of km, positive identification is usually based on opportunistic RF transmissions which contain some form of information for deducing the ID.
A common approach for identifying a spacecraft is to either directly identify by its transmission or via elimination of other object transmissions on the same launch.
In addition to that, radars have limited ability to register small spacecraft (e.g. pico-satellite) or large deployments during LEOP.
From the operator's point of view, successful identification is a hard requirement for localisation and subsequent orbit determination.

## Space Situational Awareness Operations ##

For Space Situational Awareness related operations, the targeted identification and localisation of a known spacecraft as described above, are not enough.
An additional use case for SSA operations is the ability for any spacecraft, without prior knowledge of ever being launched, to be identified and localised.
This group includes the Regulators and Space Traffic Management players and their use cases.

## List of Use Cases ##

{{% use-case %}}
